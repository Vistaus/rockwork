# French translation for upebble
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the upebble package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: upebble\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-21 05:09+0000\n"
"PO-Revision-Date: 2016-02-28 17:26+0000\n"
"Last-Translator: Anne <anneonyme017@openmailbox.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-06-14 06:29+0000\n"
"X-Generator: Launchpad (build 18097)\n"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:16
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:14
msgid "App Settings"
msgstr "Paramètres de l'application"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:136
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:53
msgid "Cut"
msgstr "Couper"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:148
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:59
msgid "Copy"
msgstr "Copier"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:160
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:65
msgid "Paste"
msgstr "Coller"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:173
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:197
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:196
msgid "Close"
msgstr "Fermer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:9
msgid "App details"
msgstr "Détails de l'application"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Install"
msgstr "Installer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Installing..."
msgstr "Installation..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Installed"
msgstr "Installées"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:235
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:235
msgid "Description"
msgstr "Description"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:258
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:258
msgid "Developer"
msgstr "Développeur"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:266
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:266
msgid "Version"
msgstr "Version"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:8
msgid "Add new watchapp"
msgstr "Ajouter une nouvelle application pour montre"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:8
msgid "Add new watchface"
msgstr "Ajouter une nouveau cadran"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:125
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:125
msgid "See all"
msgstr "Tout voir"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailySleepGraph.qml:127
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailySleepGraph.qml:127
msgid "Oops! No sleep data."
msgstr "Oups ! Aucune données sur le sommeil."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:118
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:118
msgid "TODAY'S STEPS"
msgstr "PAS EFFECTUÉS AUJOURD'HUI"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:120
msgid "YESTERDAY'S STEPS"
msgstr "PAS EFFECTUÉS HIER"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:122
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:122
msgid "STEPS"
msgstr "PAS"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:125
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:125
msgid "TYPICAL"
msgstr "TYPIQUE"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:8
msgid "Developer Tools"
msgstr "Outils pour développeurs"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:27
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:11
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:27
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:11
msgid "Screenshots"
msgstr "Captures d'écran"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:34
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:34
msgid "Install app or watchface from file"
msgstr "Installer une application ou un cadran à partir d'un fichier"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:85
msgid "Firmware upgrade"
msgstr "Mise à niveau du micrologiciel"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:16
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:16
msgid "A new firmware upgrade is available for your Pebble smartwatch."
msgstr ""
"Une nouvelle mise à niveau du micrologiciel est disponible pour votre montre "
"connectée Pebble."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:23
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:23
#, qt-format
msgid "Currently installed firmware: %1"
msgstr "Micrologiciel actuellement installé : %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:29
#, qt-format
msgid "Candidate firmware version: %1"
msgstr "Version candidate du logiciel : %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:35
#, qt-format
msgid "Release Notes: %1"
msgstr "Notes de version : %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:41
msgid "Important:"
msgstr "Important :"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:41
msgid ""
"This update will also upgrade recovery data. Make sure your Pebble "
"smartwarch is connected to a power adapter."
msgstr ""
"Cette mise à jour mettra également à niveau les données de récupération. "
"Assurez-vous que votre montre Pebble est connectée à un adaptateur "
"électrique."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthInfoItem.qml:224
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthInfoItem.qml:224
msgid "KM"
msgstr "KM"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthInfoItem.qml:263
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthInfoItem.qml:263
msgid "M"
msgstr "M"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthPage.qml:7
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthPage.qml:7
msgid "Health info"
msgstr "Informations de Santé"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:9
msgid "Health settings"
msgstr "Paramètres de Santé"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:17
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:17
msgid "Health app enabled"
msgstr "L'application Santé est activée"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:28
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:28
msgid "Female"
msgstr "Femme"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:28
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:28
msgid "Male"
msgstr "Homme"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:34
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:34
msgid "Age"
msgstr "Âge"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:47
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:47
msgid "Height (cm)"
msgstr "Taille (cm):"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:60
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:60
msgid "Weight"
msgstr "Masse"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:73
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:73
msgid "I want to be more active"
msgstr "Je veux être plus actif"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:84
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:84
msgid "I want to sleep more"
msgstr "Je veux dormir plus"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/AlertDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:14
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/AlertDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:14
msgid "OK"
msgstr "Valider"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:110
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:20
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:110
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:20
msgid "Cancel"
msgstr "Annuler"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ImportPackagePage.qml:7
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ImportPackagePage.qml:7
msgid "Import watchapp or watchface"
msgstr "Importer une application ou un cadran"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:35
#, qt-format
msgid "Version %1"
msgstr "Version %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:45
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:45
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:8
msgid "Report problem"
msgstr "Signaler un problème"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:56
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:56
msgid "Legal"
msgstr "Informations légales"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:78
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:78
msgid ""
"This application is neither affiliated with nor endorsed by Pebble "
"Technology Corp."
msgstr ""
"Cette application n'est ni affiliée, ni approuvée par Pebble Technology Corp."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:83
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:83
msgid "Pebble is a trademark of Pebble Technology Corp."
msgstr "Pebble est une marque déposée par Pebble Technology Corp."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Apps & Watchfaces"
msgstr "Applications & Cadrans"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Apps"
msgstr "Applications"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Watchfaces"
msgstr "Cadrans"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:167
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:167
msgid "Launch"
msgstr "Lancer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:177
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:176
msgid "Configure"
msgstr "Configurer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:187
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:91
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:186
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:91
msgid "Delete"
msgstr "Supprimer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/LoadingPage.qml:42
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/Main.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/LoadingPage.qml:42
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/Main.qml:95
msgid "Loading..."
msgstr "Chargement..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:15
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:15
msgid "About"
msgstr "À propos"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:22
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:22
msgid "Developer tools"
msgstr "Outils de développement"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:53
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:53
msgid "Manage notifications"
msgstr "Gérer les notifications"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:60
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:60
msgid "Manage Apps"
msgstr "Gérer les applications"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:69
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:68
msgid "Manage Watchfaces"
msgstr "Gérer les cadrans"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:89
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:76
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:8
msgid "Settings"
msgstr "Paramètres"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:205
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:191
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:36
msgid "Connected"
msgstr "Connectée"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:205
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:191
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:36
msgid "Disconnected"
msgstr "Déconnectée"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:218
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:204
msgid ""
"Your Pebble smartwatch is disconnected. Please make sure it is powered on, "
"within range and it is paired properly in the Bluetooth System Settings."
msgstr ""
"Votre montre Pebble est déconnectée. Veuillez vous assurer qu'elle est "
"allumée, à proximité et appairée correctement dans les Paramètres système / "
"Bluetooth."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:228
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:64
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:214
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:64
msgid "Open System Settings"
msgstr "Ouvrir les paramètres système"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:236
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:222
msgid "Your Pebble smartwatch is in factory mode and needs to be initialized."
msgstr ""
"Votre montre connectée Pebble est en mode usine et a besoin d'être "
"initialisée."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:245
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:231
msgid "Initialize Pebble"
msgstr "Initialiser Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:277
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:263
msgid "Upgrading..."
msgstr "Mise à niveau..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklyStepsGraph.qml:108
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklyStepsGraph.qml:108
msgid "AVERAGE STEPS"
msgstr "PAS MOYENS"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklyStepsGraph.qml:108
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklyStepsGraph.qml:108
#, qt-format
msgid "TYPICAL %1"
msgstr "TYPIQUE %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/NotificationsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/NotificationsPage.qml:8
msgid "Notifications"
msgstr "Notifications"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/NotificationsPage.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/NotificationsPage.qml:29
msgid ""
"Entries here will be added as notifications appear on the phone. Selected "
"notifications will be shown on your Pebble smartwatch."
msgstr ""
"Des entrées seront ajoutées ici lorsque des notifications apparaîtront sur "
"le téléphone. Les notifications sélectionnées seront affichées sur votre "
"montre connectée Pebble."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:6
msgid "Manage Pebble Watches"
msgstr "Gérer les montres Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:57
msgid ""
"No Pebble smartwatches configured yet. Please connect your Pebble smartwatch "
"using System Settings."
msgstr ""
"Aucune montre connectée Pebble encore configurée. Veuillez connecter votre "
"montre Pebble en utilisant les Paramètres système."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:69
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:69
msgid "Screenshot options"
msgstr "Options de capture d'écran"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:74
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:74
msgid "Share"
msgstr "Partager"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:77
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:85
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:77
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:85
msgid "Pebble screenshot"
msgstr "Capture d'écran de Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:82
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:82
msgid "Save"
msgstr "Enregistrer"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:18
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:18
msgid "Preparing logs package..."
msgstr "Préparation de l'ensemble des rapports..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:29
msgid "pebble.log"
msgstr "pebble.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:36
msgid "Send rockworkd.log"
msgstr "Envoi de rockworkd.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:41
msgid "rockworkd.log"
msgstr "rockworkd.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:46
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:46
msgid "Send watch logs"
msgstr "Envoi des rapports"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:19
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:19
msgid "Distance Units"
msgstr "Unités de distance"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:35
msgid "Metric"
msgstr "Métrique"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:48
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:48
msgid "Imperial"
msgstr "Impériale"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:55
msgid "Calendar"
msgstr "Agenda"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:62
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:62
msgid "Sync calendar to timeline"
msgstr "Synchroniser l'agenda sur le calendrier."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklySleepGraph.qml:154
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklySleepGraph.qml:154
msgid "AVERAGE SLEEP"
msgstr "SOMMEIL MOYEN"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklySleepGraph.qml:154
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklySleepGraph.qml:154
#, qt-format
msgid "TYPICAL %1H %2M"
msgstr "TYPIQUE %1H %2M"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/AlertDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/AlertDialog.qml:5
msgid "App Settings Alert"
msgstr "Paramètres de l'application - Alerte"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:5
msgid "App Settings Confirm Navigation"
msgstr "Paramètres de l'application - Confirmation de navigation"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:8
msgid "Leave"
msgstr "Quitter"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:13
msgid "Stay"
msgstr "Rester"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:5
msgid "App Settings Confirmation"
msgstr "Paramètres de l'application - Confirmation"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:5
msgid "App Settings Prompt"
msgstr "Paramètres de l'application - Demande"

#, fuzzy
#~ msgid "Health"
#~ msgstr "Informations de Santé"
